# Importing the required libraries
import xml.etree.ElementTree as ET
from xml.dom import minidom
import pandas as pd

cols = ["uid", "firstName", "lastName", "fullName", "program", "country", "city"]
rows = []

xml = open('consolidated.xml', 'r').read()
root = ET.fromstring(xml)

prefix = '{http://tempuri.org/sdnList.xsd}'

def get_info(element):
	firstName = ''
	lastName = ''
	country = ''
	city = ''
	uid = int(el.find(prefix + 'uid').text)

	### Getting program
	try:
		program = el.find(prefix + 'programList').find(prefix + 'program').text
	except Exception as e:
		print('ERROR While getting program - ', e)

	### Getting country and city
	try:
		adressess = el.find(prefix + 'addressList').findall(prefix + 'address')
		for adress in adressess:
			try:
				country = adress.find(prefix + 'country').text
				city = adress.find(prefix + 'city').text
				break
			except Exception as e:
				#print(uid, 'ERROR while getting country', e)
				pass
	except Exception as e:
		#print(uid, 'ERROR while getting country - ', e)
		pass

	### Getting firstname
	try:
		firstName = el.find(prefix + 'firstName').text
	except Exception as e:
		pass
		#print('ERROR While getting firstName - ', e)

	### Getting lastname
	try:
		lastName = el.find(prefix + 'lastName').text
	except Exception as e:
		print('ERROR While getting lastName - ', e)
	
	#fullName = " ".join([firstName, lastName])
	fullName = firstName + " " + lastName
	return [uid, firstName, lastName, fullName, program, country, city]


for el in root.findall(prefix + 'sdnEntry'):
	sdnType = el.find(prefix + 'sdnType').text.lower()
	try:
		each_data = get_info(el)
		rows.append(dict((cols[i], each_data[i]) for i in range(len(cols))))
	except Exception as e:
		print('ERROR In main loop - ', e)

df = pd.DataFrame(rows, columns=cols)

# Writing dataframe to csv
df.to_csv('output.csv', index=False)


